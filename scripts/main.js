"use strict";

//services section

const tabsServices = document.querySelectorAll(".main-services-container .items");
console.log(tabsServices);

tabsServices.forEach((tab) => {
  tab.addEventListener("click", tabChange);
});

function tabChange({ target }) {
  const currentTab = target.closest(".services-items-title");
  if (!currentTab) return;
  const tabsContainer = currentTab.closest(".main-services-container");
  const activeTabId = currentTab.dataset.tabId;
  const toggleActiveState = (element) => {
    element.dataset.tabId !== activeTabId
      ? element.classList.remove("active")
      : element.classList.add("active");
  };
  tabsContainer.querySelectorAll(".services-items-title").forEach((tab) => {
    toggleActiveState(tab);
  });
  tabsContainer.querySelectorAll(".services-items-info").forEach((content) => {
    toggleActiveState(content);
  });
}

//work section 


//feedback section



